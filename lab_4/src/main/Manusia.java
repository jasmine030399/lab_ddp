
public class Manusia {
	private int umur ;
	private String nama ;
	private int uang = 50000 ;
	private float kebahagiaan = 50;
	
	public Manusia(String nama, int umur){
		this.nama = nama;
		this.umur = umur;
	}
	
	public Manusia(String nama, int umur, int uang){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
	}
	
	public Manusia(int umur, String nama, int uang, float kebahagiaan){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = kebahagiaan;
	}
	
	public void setnama(String nama){
		this.nama = nama ;
	}
	
	public String getnama(){
		return nama;
	}
	
	
	public void setumur(int umur){
		this.umur = umur ;
	}
	
	public int getumur(){
		return umur;
	}
	
	public void setuang(int uang){
		this.uang = uang;
	}
	
	public int getuang(){
		return uang;
	}
	
	public void setkebahagiaan(float kebahagiaan)
	{
		if (kebahagiaan >= 100){
			this.kebahagiaan = 100 ;
		}else if(kebahagiaan <= 0){
			this.kebahagiaan = 0;
		}
		this.kebahagiaan = kebahagiaan;
	}
	
	public float getkebahagiaan(){ 
		return kebahagiaan;
	}
	
	public void beriUang(Manusia penerima)
	{
		int aski = 0;
		for (int i = 0 ; i < penerima.nama.length() ; i++)
		{
			char name = penerima.nama.charAt(i);
			int angka = (int) name ;
			aski += angka ;
		}
		int Jumlah_uang = aski * 100;
		
		if (Jumlah_uang <  this.uang){
			this.uang-= Jumlah_uang;
			penerima.uang += Jumlah_uang ;
			float kebahagiaanbaru = this.getkebahagiaan() + (float)(Jumlah_uang / 6000);
			this.setkebahagiaan(kebahagiaanbaru);
			kebahagiaanbaru = this.getkebahagiaan() + (float)(Jumlah_uang / 6000);
			penerima.setkebahagiaan(kebahagiaanbaru);
			
			if (kebahagiaanbaru > 100){ 
				kebahagiaanbaru = 100;
				this.setkebahagiaan(kebahagiaanbaru);
			}
			System.out.println(this.getnama() + " memberi uang sebanyak " + Jumlah_uang +  " kepada" + penerima.getnama() + ", mereka berdua senang :D ");
		}else{
			System.out.println(this.getnama() + " ingin memberi uang kepada " + penerima.getnama() + " namun tidak memilki cukup uang " );
		} 
	}
	

	public void beriUang(Manusia penerima, int Jumlah_uang)
	{
		if (Jumlah_uang <  this.uang){
			this.uang -= Jumlah_uang;
			penerima.uang += Jumlah_uang;
			float kebahagiaanbaru = this.getkebahagiaan() + (float) (Jumlah_uang / 6000) ;
			this.setkebahagiaan(kebahagiaanbaru);
			kebahagiaanbaru = this.getkebahagiaan() + (float) (Jumlah_uang / 6000) ;
			penerima.setkebahagiaan(kebahagiaanbaru);
			
			if (kebahagiaanbaru > 100){ 
				kebahagiaanbaru = 100;
				this.setkebahagiaan(kebahagiaanbaru);
			}
			System.out.println(this.nama + " memberi uang sebanyak " + Jumlah_uang +  " kepada" + penerima.nama + ", mereka berdua senang :D ");
			
		}else{
			System.out.println(this.nama + " ingin memberi uang kepada " + penerima.nama + " namun tidak memilki cukup uang " );
		} 
		
	}
	
	public void bekerja(int durasi, int bebanKerja)
	{
		if (this.umur < 18){
			System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur D:");
		}else if(this.umur >= 18){
			int BebanKerjaTotal = durasi * bebanKerja;
			int pendapatan = 0;
			if (BebanKerjaTotal <= this.kebahagiaan){
				pendapatan += BebanKerjaTotal* 10000;
				float kebahagiaanbaru = this.getkebahagiaan() - (float) BebanKerjaTotal;
				this.setkebahagiaan(kebahagiaanbaru);
				System.out.println(this.nama +" bekerja full time, total pendapatan : " + pendapatan);
			}else if(BebanKerjaTotal > this.kebahagiaan){
				float DurasiBaru = this.getkebahagiaan() /(float)bebanKerja; 
				int Durasi_Baru = (int) DurasiBaru;
				BebanKerjaTotal = Durasi_Baru * bebanKerja;
				float kebahagiaanbaru = this.getkebahagiaan() - (float) BebanKerjaTotal;
				this.setkebahagiaan(kebahagiaanbaru);
				pendapatan += BebanKerjaTotal * 10000; 
				System.out.println(this.nama + "tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : "+pendapatan);
			} 
			this.uang += pendapatan;
		
		}	
	
	
 	}

	
	public void rekreasi(String namaTempat){
		int Biaya = namaTempat.length() * 10000;
		if (this.uang > Biaya){
			this.uang-= Biaya;
			float kebahagiaanbaru = this.getkebahagiaan()+ (float)namaTempat.length();
			this.setkebahagiaan(kebahagiaanbaru);
			System.out.println(this.getnama() + " berekreasi di " + namaTempat + " ," + this.getnama() + " senang :) ");
		}else{
			System.out.println( this.getnama() +" tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
		}
	}
	
	public void sakit(String namaPenyakit){
		float kebahagiaanbaru = this.getkebahagiaan() - (float) namaPenyakit.length();
		this.setkebahagiaan(kebahagiaanbaru);
		if (kebahagiaanbaru < 0){
			kebahagiaanbaru = 0;
			this.setkebahagiaan(kebahagiaanbaru);
		}
		System.out.println(this.getnama() + " terkena penyakit" + namaPenyakit + " :O");
	}
	
	public String toString(){
		return ("nama\t\t :" + this.nama+ "\nUmur\t\t :" + this.umur + "\nUang\t\t :" + this.uang + "\nKebahagiaan\t\t :"  + this.kebahagiaan);
	}
}



	
	
			
	
