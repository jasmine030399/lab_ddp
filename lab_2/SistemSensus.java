import java.util.Scanner;
public class SistemSensus 
{
	public static void main(String[] args) 
	{
	// Buat input scanner baru
	Scanner input = new Scanner(System.in);


	// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
	// User Interface untuk meminta masukan
	System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
			"--------------------\n" +
			"Nama Kepala Keluarga   : ");
	String nama = input.nextLine();
	System.out.print("Alamat Rumah           : ");
	String alamat = input.nextLine();
	System.out.print("Panjang Tubuh (cm)     : ");
	short panjang = input.nextShort();
	if (panjang < 0 || panjang >= 250){
		System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
		return;
	}
	System.out.print("Lebar Tubuh (cm)       : ");
	short lebar = input.nextShort();
	if (lebar < 0 || lebar >= 250){
		System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
		return;
	}
	System.out.print("Tinggi Tubuh (cm)      : ");
	short tinggi = input.nextShort() ;
	if (tinggi < 0 || tinggi >= 250){
		System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
		return;
	}
	System.out.print("Berat Tubuh (kg)       : ");
	float berat = input.nextFloat() ;
	if (berat < 0 || berat >= 150){
		System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
		return;
	}
	System.out.print("Jumlah Anggota Keluarga: ");
	int makanan = input.nextInt();
	if (makanan < 0 || makanan >= 20){
		System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
		return;
	}
	System.out.print("Tanggal Lahir          : ");
	String tanggalLahir = input.next() ;
	String[] tanggalLahir2 = tanggalLahir.split("-");
	short tanggalLahir3 = Short.parseShort(tanggalLahir2[2]);
	if (tanggalLahir3 < 1000 || tanggalLahir3 > 2018){
		System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
		return;
	}
	System.out.print("Catatan Tambahan       : ");
	input.nextLine();
	String catatan = input.nextLine();
	System.out.print("Jumlah Cetakan Data    : ");
	int JumlahCetakan = input.nextInt() ;
	if (JumlahCetakan >= 99){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			return;
	}
	input.nextLine();




// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
// TODO Hitung rasio berat per volume (rumus lihat soal)
	double rasio1 = (berat)/(panjang * lebar * tinggi * 0.000001);
	//int rasio1 = (int) rasio ;  
	for (int i = 1 ; i > 0 ; i++){
		if (i == JumlahCetakan + 1){
			break;
		}
		// TODO Minta masukan terkait nama penerima hasil cetak data
		System.out.print("Pencetakan " + i + " dari " + JumlahCetakan + " untuk: " );
		String penerima = input.nextLine().toUpperCase() ;  // Lakukan baca input lalu langsung jadikan uppercase
		System.out.println("DATA SIAP DICETAK UNTUK\t" + penerima );
		System.out.println("---------------------------------------");
		System.out.println(nama+ '-' +alamat);
		System.out.println("Lahir pada tanggal\t" + tanggalLahir);
		System.out.println("Rasio Berat Per Volume	 = " + rasio1 + "\tkg/m^3");
		
		
			if (catatan.length() > 1){
				System.out.println(catatan);}
			else{
				System.out.println("tidak ada catatan tambahan");
			}
				
			
			// TODO Periksa ada catatan atau tidak

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
	
	}
	
		}

		


		
}



	



 






		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		//String rekomendasi = "";
		//.....;

		//input.close();

