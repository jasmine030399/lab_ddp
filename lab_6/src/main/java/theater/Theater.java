//Put your Theater class here, and don't forget to write "package theater" at the top of your program
package theater;
import java.util.ArrayList;
import java.util.Arrays;
import movie.Movie;
import ticket.Ticket;

public class Theater{
	private String bioskop;
	private int saldo;
	private ArrayList<Ticket> jumlahtiket;
	private Movie[] daftarfilm;
	
	public Theater(String bioskop, int saldo, ArrayList<Ticket> jumlahtiket,Movie[] daftarfilm){
		this.bioskop = bioskop;
		this.saldo = saldo;
		this.jumlahtiket = jumlahtiket;
		this.daftarfilm = daftarfilm;
	}
	
	public void setBioskop(String bioskop){
		this.bioskop = bioskop;
	}
	
	public void setSaldo(int saldo){
		this.saldo = saldo;
	}
	
	public void setJumlahtiket(ArrayList<Ticket> jumlahtiket){
		this.jumlahtiket = jumlahtiket;
	}
	
	public void setDaftarfilm(Movie[] daftarfilm){
		this.daftarfilm = daftarfilm;
	}
	
	public String getBioskop(){
		return bioskop;
	}

	public int getSaldo(){
		return saldo;
	}
	
	public ArrayList<Ticket> getJumlahtiket(){
		return jumlahtiket;
	}
	
	public Movie[] getDaftarfilm(){
		return daftarfilm;
	}
	
	
		
	public void printInfo(){
		String judulfilm = "";
		for(int i = 0; i < getDaftarfilm().length; i++){
			judulfilm += getDaftarfilm()[i].getJudul() + ",";
			
		} 
		
		System.out.println("--------------------------------------------------------------------\n" +
				"\nBioskop				: " + this.getBioskop() + "\n" +
				"\nSaldo Kas			: " + this.getSaldo() + "\n" +
				"\nJumlah tiket tersedia 		: " + this.getJumlahtiket().size() + "\n" +
				"\nDaftar Film Tersedia		: " + judulfilm + "\n" + 
				"--------------------------------------------------------------------");
	
	}

	public static void printTotalRevenueEarned(Theater[] theaters){
		int pendapatan = 0;
		for(int i = 0; i<theaters.length; i++){
			pendapatan = pendapatan + theaters[i].getSaldo();
		System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + );
		}
		for(int j = 0; j<theaters.length ; j++){
			System.out.println("--------------------------------------------------------------------\n" + 
							   "Bioskop\t\t:" + theaters[j].getBioskop()+ "\n" +
							   "Saldo Kas\t\t: Rp. " + theaters[j].getSaldo() +"\n");
		}
		
	}
	
}