package ticket;
import movie.Movie; 
public class Ticket{
	private Movie judul;
	private String jadwal;
	private boolean jenis;
	
	public Ticket(Movie judul, String jadwal, boolean jenis){
		this.judul = judul;
		this.jadwal = jadwal;
		this.jenis = jenis;
	}
	
	public void setJudul(Movie judul){
		this.judul = judul;
	}
	
	public void setJadwal(String jadwal){
		this.jadwal = jadwal;
	}
	
	public void setJenis(boolean jenis){
		this.jenis = jenis;
	}

	public Movie getJudul(){
		return judul;
	}
		
	public String getJadwal(){
		return jadwal;
	}
	
	public boolean getJenis(){
		return jenis;
	}
	
	public int hitungHarga(){
		
		if (this.getJenis() == false){
			if(this.jadwal.equals("Sabtu") || this.jadwal.equals("Minggu")){
				return 100000;	
			}else{
				return 60000;
			}
		}else{
			if(this.jadwal.equals("Sabtu") || this.jadwal.equals("Minggu")){
				return 100000 + (100000 *(20/100));
			}else{
				return 60000 + (60000 * (20/100));
			}
		}
				
	}
	
	
	public String InfoTicket(){
		String isJenis = "";
		if (this.getJenis() == true){
			isJenis = "3 Dimensi";
		}else{
			isJenis = "2 Dimensi";
		}
			
		return ("--------------------------------------------------------------------" + "\n" +
				"Film			: " + this.judul.getJudul() + "\n" +
				"Jadwal Tayang	: " + this.getJadwal() + "\n" +
				"Jenis			: " + isJenis + "\n" +
				"--------------------------------------------------------------------" + "\n");
	}
}