package movie;
public class Movie{
	private String judul;
	private String genre;
	private int durasi;
	private String rating;
	private String jenis;
	
	public Movie(String judul, String genre, int durasi, String rating, String jenis){
		this.judul = judul;
		this.genre = genre;
		this.durasi = durasi;
		this.rating = rating;
		this.jenis =jenis;
	}
	
	public void setJudul(String judul){
		this.judul = judul;
	}
	
	public String getJudul(){
		return judul;
	}
	
	public void setGenre(String genre){
		this.genre = genre;
	}
	
	public String getGenre(){
		return genre;
	}
	
	public void setDurasi(int durasi){
		this.durasi = durasi;
	}
	
	public int getDurasi(){
		return durasi;
	}
	
	public void setRating(String rating){
		this.rating = rating;
	}
	
	public String getRating(){
		return rating;
	}
	
	public void setJenis(String jenis){
		this.jenis = jenis;
	}
	
	public String getJenis(){
		return jenis;
	}
	
	public String MovieInfo(){
		return ("--------------------------------------------------------------------" + "\n" +
				"Judul	: " + this.getJudul() + "\n" +
				"Genre	: " + this.getGenre() + "\n" +
				"Durasi	: " + this.getDurasi() + "\n" +
				"Rating	: " + this.getRating() + "\n" +
				"Jenis	: " + this.getJenis() + "\n" +
				"--------------------------------------------------------------------" + "\n");
	}
}
	
	
	
