import java.util.Scanner;

public class DataInput{
	public static void main(String []args){
		Scanner input = new Scanner(System.in);
		Number [][] kotak = new Number[5][5];
		Number[] states = new Number[100];
	
		for(int i = 0 ; i < 5; i++){
			String[] masukan = input.nextLine().split(" ");
			for(int j = 0 ; j<5 ; j++){
				kotak[i][j] = new Number(Integer.parseInt(masukan[j]),i,j);
				states[Integer.parseInt(masukan[j])] = kotak[i][j];
			}
		}
		BingoCard player = new BingoCard(kotak, states);
		while(true){
			String[] command = input.nextLine().split(" ");
			if (command[0].equals("MARK")){
				System.out.println(player.markNum(Integer.parseInt(command[1])));
				if(player.isBingo() == true){
					System.out.println("BINGO!");
					break;
				}
			}else if(command[0].equals("INFO")){
				System.out.println(player.info());
			}else if(command[0].equals("RESTART")){
				player.restart();
			}
		}
	}
}
	