/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		//TODO Implement
		if (numberStates[num] == null){
			return ("kartu tidak memiliki angka " + num);
		}else{
			if (numberStates[num].isChecked() == false){
				numberStates[num].setChecked(true);
				cekBingo();
				return (num + " tersilang");
			}else{
				return (num + " sebelumnya sudah tersilang");
			}
		}
	}	
	
	public String info(){
		//TODO Implement
		String output = "";
		for(int i = 0 ; i < 5 ; i++){
			for(int j = 0 ; j<5 ; j++){
				if(numbers[i][j].isChecked() == false){
					output = output + " | " + numbers[i][j].getValue() + " ";
					if (j == 4){
						output = output + " | ";
						if (i<4){
							output += "\n";
						}
					}	
				}else{
					output += " | X" ;
					if (j == 4){
						output = output + " | ";
						if (i<4){
							output += "\n";
						}
					}
				}
			}
		}
		return output;
	}
	
	
	public void cekBingo(){
		for(int i = 0 ; i < 5 ; i++){
			for(int j = 0 ; j<5 ; j++){
				if((numbers[0][j].isChecked() == true) && (numbers[1][j].isChecked() == true) && (numbers[2][j].isChecked() == true) && (numbers[3][j].isChecked() == true) && (numbers[4][j].isChecked() == true)){
					this.setBingo(true);
				}if((numbers[i][0].isChecked() == true) && (numbers[i][1].isChecked() == true) && (numbers[i][2].isChecked() == true) && (numbers[i][3].isChecked() == true) && (numbers[i][4].isChecked() == true)){
					this.setBingo(true); 
				}if((numbers[0][0].isChecked() == true) && (numbers[1][1].isChecked() == true) && (numbers[2][2].isChecked() == true) && (numbers[3][3].isChecked() == true) && (numbers[4][4].isChecked() == true)){
					this.setBingo(true);
				}if((numbers[0][4].isChecked() == true) && (numbers[1][3].isChecked() == true) && (numbers[2][2].isChecked() == true) && (numbers[3][1].isChecked() == true) && (numbers[4][0].isChecked() == true)){
					this.setBingo(true);
				}else{
					this.setBingo(false);
				}
			}
		}
	}
			
			
			
				
		
	public void restart(){
		//TODO Implement
		for(int i = 0; i< 5; i++){
			for(int j = 0 ; j < 5; j++){
				if(numbers[i][j].isChecked() == true){
					numbers[i][j].setChecked(false);
				}
			}
		}
		System.out.println("Mulligan!");
	}
	

}
